meta-репозиторий для Yocto - https://gitlab.com/autogramma_multimedia_rockpi/meta-radxa

kernel - https://gitlab.com/autogramma_multimedia_rockpi/kernel

Инструкция для сборки:
1. Установка необходимых пакетов (только один раз):
sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
     build-essential chrpath socat cpio python3 python3-pip python3-pexpect \
     xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
     pylint3 xterm

2. Установка repo (только один раз):
	
	curl https://storage.googleapis.com/git-repo-downloads/repo | sudo tee /usr/bin/repo

	sudo chmod a+x /usr/bin/repo

	После этого необходимо в скрипте repo изменить первую строчку с #!/usr/bin/env python на #!/usr/bin/env python3

3. Создание рабочей папки:
	
	mkdir ~/Your_working_folder

	cd ~/Your_working_folder

4. Инициализация и синхронизация repo

	repo init -u https://gitlab.com/autogramma_multimedia_rockpi/linux_base -b main -m manifest.xml
	
	repo sync -j$(nproc)

5. Настройка среды 

	cd poky

	source oe-init-build-env
	
	cd ..
	
	cp meta-radxa/conf/bblayers.conf.sample build/conf/bblayers.conf
	
	cp meta-radxa/conf/local.conf.sample build/conf/local.conf

6. Модификация файла build/conf/local.conf 

	Для ROCK Pi 4A необходимо раскомментировать строчку: MACHINE ?= "rockpi-4a-rk3399"

	Для ROCK Pi 4B необходимо раскомментировать строчку: MACHINE ?= "rockpi-4b-rk3399"

	Для ROCK Pi 4C необходимо раскомментировать строчку: MACHINE ?= "rockpi-4c-rk3399"


7. Сборка (занимает длительное время)

	source oe-init-build-env

	bitbake -k radxa-minimal-image - минимальный вариант

	bitbake -k radxa-console-image - консольный вариант 

	bitbake -k radxa-desktop-image - варинт с GUI

	*Не рекомендуется прерывать сборку, это может привести к возникновению ошибок.

	*При возникновении ошибок необходимо возобновнить сборку командой bitbake -k radxa-console-image
	
	*После завершения сборки образ находится по следующему пути source/build/tmp/deploy/images/rockpi-4c-rk3399/radxa-console-image-rockpi-4c-rk3399-gpt.img

8. Запись образа осуществляется с помощью утилиты balenaEtcher, которую можно скачать на сайте https://wiki.radxa.com/Rockpi4/downloads в разделе "Tools" или на сайте https://www.balena.io/etcher. 


Полезные ссылки: 

Build mainline kernel(5.x) - https://wiki.radxa.com/Rockpi4/dev/kernel-mainline

Yocto Layer for Radxa Boards - https://wiki.radxa.com/Yocto-layer-for-radxa-boards 

